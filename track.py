import math

class Track:
    def distance(self, data1, data2):
        return math.sqrt(pow(data1['x']-data2['x'],2)+pow(data1['y']-data2['y'],2))

    def get_city_distance(self, start_city, data):
        if len(data) < 2:
            return [start_city]
        city_position = data[start_city]
        city_distance = {}
        temp_distance = {}
        for city in data:
            if city == start_city:
                continue
            if city not in temp_distance:
                temp_distance[city] = {}
            temp_distance[city] = self.distance(city_position, data[city])
        city_distance[start_city] = temp_distance
        return city_distance

    def show_track(self, start_point, data):
        city_order = []
        try:
            count_all_distance = {}
            for city in data:
                city_distance = self.get_city_distance(city, data)
                count_all_distance.update(city_distance)
            city_order = [start_point]
            next_city = start_point
            min_city = None
            while len(city_order) < len(data):
                min_distance = None
                for city in count_all_distance[next_city]:
                    if (
                        (min_distance is None
                        or min_distance > count_all_distance[next_city][city])
                        and city not in city_order
                    ):
                        min_distance = count_all_distance[next_city][city]
                        min_city = city
                next_city = min_city
                city_order.append(next_city)
        except Exception as e:
            pass
        return city_order
