from track import Track

data = {
    'A': {
        'x': 5,
        'y': 0,
    },
    'B': {
        'x': 15,
        'y': 0,
    },
    'C': {
        'x': 5,
        'y': 15,
    },
    'D': {
        'x': 15,
        'y': 15,
    },
}

track = Track()
city_track = track.show_track('Z ', data)
print(city_track)

""" input: A """
""" output:  ['A', 'B', 'D', 'C'] """
